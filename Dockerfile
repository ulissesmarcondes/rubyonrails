FROM ruby:2.4.5

Run apt-get update -qq && apt-get install -y build-essencial libpq-dev nodejs

Run mkdir /myadpp

workdir /myapp

COPY Gemfile /myapp/Gemfile

COPY  GemFile.lock /myapp/Gemfile.lock

RUN bundle install

COPY . /myapp